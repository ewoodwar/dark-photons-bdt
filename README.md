# Dark Photons BDT

This Boosted Decision Tree (BDT) takes in dark photon pair production MC as signal, and ATLAS data from 2022 as background. The goal is to use measured variables from the ATLAS tracker and calorimeter to distinguish signal reconstructed by ATLAS as electrons and photons from background electron and photons.

## Suggested Reading

This BDT was made using TMVA, the [user's guide](https://root.cern.ch/download/doc/tmva/TMVAUsersGuide.pdf) is a good resource. In particular section 3: using TMVA describes the setup, with Table 2 giving a list of training configuration options.

The user's guide section 8.13 describes what a Boosted Decision Tree is and how it works.

## Input files

If you are working on xenia (the Nevis cluster) the microntuples are stored under `/data/users/elw2154/DarkPhotonDV/ntuples/`.
The microntuples are stored on lxplus (CERN cluster) under  `/eos/user/e/ewoodwar/llp_studies/samples/ntuples/`. 

In the `/signal/` directory you can find a grid of signal points with varying mass and lifetime parameters. For instance `380_115_1ns.root` contains events with dark photon decays, where each dark Higgs has a mass of 380 GeV and each dark photon has a mass of 115 GeV and 1 ns mean lifetime.

In the `data` directory is a subset of ATLAS proton-proton data taken in 2022, at CoM energy 13.6TeV.

You can use `root` interactively to look inside the files:

```
root -l /eos/user/e/ewoodwar/llp_studies/samples/ntuples/signal/380_115_1ns.root 
Attaching file 380_115_1ns.root as _file0...
(TFile *) 0x37569a0
root [1] .ls
TNetXNGFile**           root://eoshome-e.cern.ch//eos/user/e/ewoodwar/llp_studies/samples/ntuples/signal/380_115_1ns.root
 TNetXNGFile*           root://eoshome-e.cern.ch//eos/user/e/ewoodwar/llp_studies/samples/ntuples/signal/380_115_1ns.root
  KEY: TTree    trees_SR_highd0_;3      trees_SR_highd0_ [current cycle]
  KEY: TTree    trees_SR_highd0_;2      trees_SR_highd0_ [backup cycle]
  KEY: TH1D     MetaData_EventCount;1   MetaData_EventCount
  KEY: TH1D     MetaData_SumW;1 MetaData_SumW
  KEY: TH1D     cutflow;1       cutflow
  KEY: TH1D     cutflow_weighted;1      cutflow_weighted
  KEY: TH1D     cutflow_electrons_1;1   cutflow_electrons_1
  KEY: TH1D     cutflow_electrons_2;1   cutflow_electrons_2
  KEY: TH1D     cutflow_muons_1;1       cutflow_muons_1
  KEY: TH1D     cutflow_muons_2;1       cutflow_muons_2
  KEY: TH1D     cutflow_photons_1;1     cutflow_photons_1
  KEY: TH1D     cutflow_taus_1;1        cutflow_taus_1
  KEY: TH1D     cutflow_taus_2;1        cutflow_taus_2
  KEY: TH1D     cutflow_jets_1;1        cutflow_jets_1
  KEY: TH1D     cutflow_trks_1;1        cutflow_trks_1
  KEY: TH1D     cutflow_truths_1;1      cutflow_truths_1
```

## Description of input variables

`electron_pt:` transverse momentum of electron track. Transverse = in plane orthogonal to direction of the proton-proton beams.

`electron_eta:` pseudorapidity coordinate of leading electron (see https://en.wikipedia.org/wiki/Pseudorapidity).

`electron_maxEcell_E:` energy deposited in calorimeter cell that recieved the most energy from this electron.

`electron_dpt:` percent difference between pt of electron track and electron object. See page 27 here (https://cds.cern.ch/record/2694014/files/ATL-COM-PHYS-2019-1321.pdf).

`electron_chi2:` goodness of fit parameter for electron track fom fit to the hits that form the track.

`electron_nPIX:` number of pixel layers crossed by the electron track.

`electron_time:` calibrated time of production of the electron as measured by the LAr calorimeter

`electron_d0:` transverse impact parameter of the electron track

Adding an integer to the end of the variable name (e.g. electron_eta[0]) tells Root which electron to consider. The index "0" in `electron_eta[0]` refers to the "leading" electron. This is the electron with the most transverse momentum in each event. `electron_eta[1]` refers to "subleading" electron, which has the second most `pt` in the event.

Please, play around and add/remove input variables! You can see what is available, for instance in the two photon final state with `trees_SR_highd0_->Print("photon*")` in interactive `root`. 

## How to run

To clone this repo on your local machine, do:

`git clone ssh://git@gitlab.cern.ch:7999/ewoodwar/dark-photons-bdt.git`

After this, run the command:

`root -q -b -l myBDT.C`  will run the BDT and produce an `output.root` file.

In the text output you will also see a ranking of the variables input by importance, and a correlation matrix for these variables for signal and for background.

You can edit `myBDT.C`, for instance with `vim myBDT.C` or any other text editor, to change the input variables/training hyperparameters such as depth etc (table 25 and 26 of the TMVA users guide for options). There are comments within `myBDT.C` that indicate where/how to change configuration+inputs.

## How to view output

`root -l -e "TMVA::TMVAGui("$put/in/here/your/path/to/tmva/output/file.root")"`

This will bring up a GUI which is described in the user's guide. Here you can find ROC curves (https://en.wikipedia.org/wiki/Receiver_operating_characteristic), classifier score, plots of input variables, etc. Some plots, and the BDT weights, are also saved to the datasets/ directory by default.
